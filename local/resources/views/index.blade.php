﻿<!DOCTYPE html>
<html lang="en" ng-app="manEmployee">
<head>
	<meta charset="UTF-8" />
	<title>LA</title>
	<link type="text/css" rel="stylesheet" href="<?php echo 'local/public/template/css/bootstrap.min.css' ;?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo 'local/public/template/css/style.css' ;?>" />
</head>
<body>
	<div class="container" ng-controller="EmployeesController">
		<center><h2>Danh Sách Nhân Viên</h2></center>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>STT</th>
					<th width="30%">Họ và Tên</th>
					<th>Tuổi</th>
					<th>Email</th>
					<th>Điện Thoại</th>
					<th>Địa Chỉ</th>
					<th width="10%"><button id="btn-add" class="btn btn-primary btn-xs" ng-click="modal('add')">Thêm Nhân Viên</button></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="employee in employees">
					<td>@{{ employee.id }}</td>
					<td>@{{ employee.name }}</td>
					<td>@{{ employee.age }}</td>
					<td>@{{ employee.email }}</td>
					<td>@{{ employee.phone }}</td>
					<td>@{{ employee.address }}</td>
					<td>
						<button class="btn btn-default btn-xs btn-detail" id="btn-edit"  ng-click="modal('edit', employee.id)">Sửa</button>
						<button class="btn btn-danger btn-xs btn-delete" ng-click="remove(employee.id)">Xóa</button>
					</td>
				</tr>
			</tbody>
		</table>
		
		<!-- Modal -->
		<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">@{{ frmtitle }}</h4>
			  </div>
			  <div class="modal-body">
				<form name="frmEmployee" class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Họ tên</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="name" name="name" placeholder="Vui lòng nhập họ tên" ng-model="employee.name" ng-required="true"/>
							<span id="helpBlock2" class="help-block" ng-show="frmEmployee.name.$error.required">Vui lòng nhập họ tên</span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Tuổi</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="age" name="age" placeholder="Vui lòng nhập tuổi" ng-model="employee.age" ng-required="true">
							<span id="helpBlock2" class="help-block" ng-show="frmEmployee.age.$error.required">Vui lòng nhập tuổi</span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Email</label>
						<div class="col-sm-9">
							<input type="email" class="form-control" id="email" name="email" placeholder="Vui lòng nhập Email"ng-model="employee.email" ng-required="true" />
							<span id="helpBlock2" class="help-block" ng-show="frmEmployee.email.$error.required">Vui lòng nhập email</span>
							<span id="helpBlock2" class="help-block" ng-show="frmEmployee.email.$error.email">Vui lòng nhập đúng định dạng email</span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Điện thoại</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="phone" name="phone" placeholder="Vui lòng nhập số điện thoại" ng-model="employee.phone" ng-required="true"/>
							<span id="helpBlock2" class="help-block" ng-show="frmEmployee.phone.$error.required">Vui lòng nhập điện thoại</span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Địa Chỉ</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="address" name="address" placeholder="Vui lòng nhập địa chỉ" ng-model="employee.address" ng-required="true"/>
							<span id="helpBlock2" class="help-block" ng-show="frmEmployee.address.$error.required">Vui lòng nhập địa chỉ</span>
						</div>
					</div>
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary" ng-disabled="frmEmployee.$invalid" ng-click="save(state,id)">Lưu</button>
			  </div>
			</div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>

	<script type="text/javascript" src="<?php echo 'local/public/template/js/jquery.min.js' ;?>"></script>
	<script type="text/javascript" src="<?php echo 'local/public/template/js/bootstrap.min.js' ;?>"></script>
	<script type="text/javascript" src="<?php echo 'local/public/app/libs/angular.min.js' ;?>"></script>
	<script type="text/javascript" src="<?php echo 'local/public/app/app.js'  ;?>"></script>
	<script type="text/javascript" src="<?php echo 'local/public/app/controllers/EmployeesController.js'  ;?>"></script>
	<script type="text/javascript" src="<?php echo 'local/public/template/js/myscript.js'  ;?>"></script>
</body>
</html>