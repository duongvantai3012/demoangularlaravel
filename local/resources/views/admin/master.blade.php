<!DOCTYPE html>
<html lang="en" ng-app="manEmployee">
<head>
	<meta charset="UTF-8" />
	<title>LA</title>
	<link type="text/css" rel="stylesheet" href="{{ asset('local/public/template/css/bootstrap.min.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('local/public/template/css/style.css') }}" />
</head>
<body>
	<div class="container">
		@include('admin.blocks.flash')
		@yield('content')
	</div>

	<script type="text/javascript" src="{{ asset('local/public/template/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('local/public/template/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('local/public/app/libs/angular.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('local/public/app/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('local/public/app/controllers/EmployeesController.js') }}"></script>
	<script type="text/javascript" src="{{ asset('local/public/template/js/myscript.js') }}"></script>


</body>
</html>