<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert(
	        [	
		     	[
		            'name' => 'Dương Văn Tài',
		            'age' => 24,
		            'email' => 'duongvantai@gmail.com',
		            'phone' => '0123456789',
		            'address' => 'Hà Tĩnh',
		            'created_at' => new DateTime()
		        ],
		        [
		            'name' => 'Trần Văn Tèo',
		            'age' => 22,
		            'email' => 'tranvanteo@gmail.com',
		            'phone' => '0123123123',
		            'address' => 'Huế',
		            'created_at' => new DateTime()
		        ],
		        [
		            'name' => 'Nguyễn Văn A',
		            'age' => 27,
		            'email' => 'nguyenvana@gmail.com',
		            'phone' => '0123789798',
		            'address' => 'Quảng Nam',
		            'created_at' => new DateTime()
		        ]
		    ]
        );
    }
}
