myApp.controller('EmployeesController', function($scope, $http, API){
	$http.get(API + 'list').success(function(response){
		$scope.employees = response;
	});

	$scope.modal = function(state, id) {
		$scope.state = state;
		if(state == "add"){
			$scope.formTitle = "Thêm Nhân Viên";
		}

		if(state == "edit"){
			$scope.formTitle = "Sửa Nhân Viên";
			$scope.id = id;
			$http.get(API + 'edit/'  + id).success(function(response){
				$scope.employee = response;
			});
		}

		$("#myModal").modal('show');
	};

	$scope.save = function(state,id) {
		if (state == "add"){
			var url = API + "add";
			var data = 	$.param($scope.employee);
			$http({
				method: "POST",
				url: url,
				data: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			})
			.success(function(response){
				console.log(response);
				location.reload();
			})
			.error(function(response){
				console.log(response);
				alert("Đã xảy ra lỗi");
			});
		}

		if (state == "edit"){
			var url = API + "edit/" + id;
			var data = 	$.param($scope.employee);
			$http({
				method: "POST",
				url: url,
				data: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			})
			.success(function(response){
				console.log(response);
				location.reload();
			})
			.error(function(response){
				console.log(response);
				alert("Đã xảy ra lỗi");
			});
		}

	};

	$scope.remove = function(id){
		var removeConfirm = window.confirm("Bạn có muốn xóa thành viên này không?");
		if (removeConfirm){
			$http.get(API + 'delete/' + id)
			.success(function(response){
				console.log(response);
				location.reload();
			})
			.error(function(){
				console.log(response);
				alert("Đã xảy ra lỗi");
			});
		}else{
			return false;
		}
	}

});