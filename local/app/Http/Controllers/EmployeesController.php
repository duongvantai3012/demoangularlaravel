<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employees;
use DateTime;

class EmployeesController extends Controller
{
    public function getList(){
    	return Employees::orderBy('id', 'DESC')->get();
    }

    public function getAdd(){

    }

    public function postAdd(Request $request){
    	$employee = new Employees;
    	$employee->name = $request->name;
    	$employee->age = $request->age;
    	$employee->email = $request->email;
    	$employee->phone = $request->phone;
    	$employee->address = $request->address;
    	$employee->created_at = new DateTime();
    	$employee->save();
    	return redirect()->route('getList')->with(['flash_level' => 'result_msg', 'flash_message' => 'Thêm Nhân Viên Thành Công']);
    }

    public function getEdit($id){
        return Employees::findOrFail($id);
    }

    public function postEdit(Request $request, $id){
        $employee = Employees::findOrFail($id);
        $employee->name = $request->name;
        $employee->age = $request->age;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->address = $request->address;
        $employee->updated_at = new DateTime();
        $employee->save();
        return redirect()->route('getList')->with(['flash_level' => 'result_msg', 'flash_message' => 'Sửa Nhân Viên Thành Công']);
    }

    public function getDelete($id) {
        $employee = Employees::findOrFail($id);
        $employee->delete();
        return redirect()->route('getList')->with(['flash_level' => 'result_msg', 'flash_message' => 'Xóa Nhân Viên Thành Công']);
    }
}
