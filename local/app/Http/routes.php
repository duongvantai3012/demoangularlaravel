<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('admin.pages.index');
});

Route::get('list', ['as' => 'getList', 'uses' => 'EmployeesController@getList']);
Route::post('add', ['as' => 'postAdd', 'uses' => 'EmployeesController@postAdd']);
Route::get('edit/{id}', ['as' => 'getEdit', 'uses' => 'EmployeesController@getEdit']);
Route::post('edit/{id}', ['as' => 'postEdit', 'uses' => 'EmployeesController@postEdit']);
Route::get('delete/{id}', ['as' => 'getDelete', 'uses' => 'EmployeesController@getDelete']);
